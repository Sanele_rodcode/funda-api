-- MySQL dump 10.11
--
-- Host: localhost    Database: funda
-- ------------------------------------------------------
-- Server version	5.0.41-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer_votes`
--

DROP TABLE IF EXISTS `answer_votes`;
CREATE TABLE `answer_votes` (
  `id` int(11) NOT NULL auto_increment,
  `answer_id` int(11) default NULL,
  `vote` int(11) default NULL,
  `user_id` int(11) default NULL,
  `username` varchar(60) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_votes`
--

LOCK TABLES `answer_votes` WRITE;
/*!40000 ALTER TABLE `answer_votes` DISABLE KEYS */;
INSERT INTO `answer_votes` VALUES (1,1,1,6,'Kawhi Leonard'),(2,46,1,5,'student%20student'),(3,5,0,5,'student%20student');
/*!40000 ALTER TABLE `answer_votes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `answer_id` int(10) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `answer` text,
  `username` varchar(255) default NULL,
  `q_id` int(11) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,2,'Oh really? :/','Namanja Nedovic',1,'0000-00-00 00:00:00'),(2,2,'Oh! Are you really?','Namanja Nedovic',2,'0000-00-00 00:00:00'),(5,6,'Hello....It\'s me.','Kawhi Leonard',1,'0000-00-00 00:00:00'),(15,1,'ssdxkfs','Sanele Mpangalala',3,'0000-00-00 00:00:00'),(16,4,'Bruh!!!','Lou Will',4,'0000-00-00 00:00:00'),(17,4,'Hi there','Lou Will',3,'0000-00-00 00:00:00'),(18,4,'Are you really?','Lou Will',2,'0000-00-00 00:00:00'),(19,4,'Oh boy!','Lou Will',2,'0000-00-00 00:00:00'),(20,4,'Was it really?','Lou Will',1,'0000-00-00 00:00:00'),(21,4,'What%27s%20that%3F','Lou Will',2,'0000-00-00 00:00:00'),(22,4,'Answer%20test','Lou%20Will',1,'0000-00-00 00:00:00'),(23,1,'This%20is%20to%20test%20the%20sockets.','Sanele%20Mpangalala',1,'0000-00-00 00:00:00'),(24,1,'Sockets%20are%20great.','Sanele%20Mpangalala',1,'0000-00-00 00:00:00'),(25,1,'Hope%20it%20works','Sanele%20Mpangalala',1,'0000-00-00 00:00:00'),(26,1,'Hey%20there%21','Sanele%20Mpangalala',1,'0000-00-00 00:00:00'),(27,1,'Please%20work','Sanele%20Mpangalala',1,'0000-00-00 00:00:00'),(28,1,'10th%20message','Sanele%20Mpangalala',1,'0000-00-00 00:00:00'),(29,1,'It%20dont%20seem%20like%20it','Sanele%20Mpangalala',2,'0000-00-00 00:00:00'),(30,1,'asda','Sanele%20Mpangalala',1,'0000-00-00 00:00:00'),(31,1,'asdaa','Sanele%20Mpangalala',2,'0000-00-00 00:00:00'),(32,1,'asda','Sanele%20Mpangalala',2,'0000-00-00 00:00:00'),(33,1,'asdasdasda','Sanele%20Mpangalala',2,'0000-00-00 00:00:00'),(34,1,'%2Ckhdsfjgjuduhgiuedhxfiuuiygiaeud','Sanele%20Mpangalala',2,'0000-00-00 00:00:00'),(35,3,'Hey%21','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(36,3,'Heyyy','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(37,3,'asda','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(38,3,'asdas','Kawhi%20Leonard',2,'0000-00-00 00:00:00'),(39,3,'asdqawsad','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(40,3,'asda','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(41,3,'Broadcast%20test','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(42,3,'Broadcast%20test%202','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(43,3,'asdasdqw','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(44,3,'asdqawdasd','Kawhi%20Leonard',1,'0000-00-00 00:00:00'),(45,3,'asdejggdjahsd','Kawhi%20Leonard',3,'0000-00-00 00:00:00'),(46,1,'asdjyahsfdjha%20jeaafsh%20gdas','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(47,1,'angsdajsgdva','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(48,1,'asdassdasd','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(49,1,'asdascsdsad','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(50,1,'Bruh%21%20Are%20you%20really%3F%3F','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(51,1,'Testing%20again','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(52,1,'Hi','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(53,1,'asjdhajkbsf','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(54,1,'Pull%20up%20to%20mi%20bumper','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(55,1,'nasdnskjdg%20jfsdldkjfhg%20k%2Csdjddfgb%20jm','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(56,1,'ammshbhvdjhabs','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(57,1,'asdnlenwneffnsca%20wdenefsd','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(58,1,'sdkfsjdbs%20sdhs%20djfs%20dfjsd%20fsjd%20ffjsd%20sdf','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(59,1,'frooo','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(60,1,'sdkfhbasakdfn%20%20wsjejhjfbsmhbjsjrk%20jkjbsesdj','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(61,1,'Hello%21%21','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(62,1,'ahdkfxuwhss sdjhfd fdfug sjdkbfkfdfs','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(64,1,'ashfgjahsgd%20%20kjajs%20rfywsjfj%20wyffaj%20ckqkasg%20rj%20oaeugejyr%20w%20hoowe%20i','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(65,3,'hsdhbvj%20hh%20sjdh%20%20jgsd%20vhb%20ufh%20gksdd','Kawhi%20Leonard',6,'0000-00-00 00:00:00'),(66,1,'askjdhbhkajsf%20kaekhsgkasb%20jeegfjz','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(67,1,'nzgdvjfhbsn','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(68,1,'Hey%20there','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(69,1,'hsbdjfhgbjs%20edfj%20skd%20ffjhs%20djm','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(70,1,'sdfsdfsd','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(71,1,'gskjdfh%20gmdnsh%20b','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(72,1,'nzaggdf%20jvd','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(73,1,'nsghdf%20','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(74,1,'mahahdsvfznhmc','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(75,1,'hsfjhsg%20jd%20vj','Sanele%20Mpangalala',6,'0000-00-00 00:00:00'),(76,1,'Hey','Sanele%20Mpangalala',6,'2019-06-15 14:54:39'),(77,5,'Hello World','student%20student',7,'2019-07-26 16:01:39'),(78,5,'nngnzmxbcvjms','student%20student',8,'2019-07-28 00:25:38'),(79,6,'What%20are%20you%20saying%20bruh%3F','Sanza%20Mananza',8,'2019-07-29 16:24:32'),(80,3,'Heyyyy','Kawhi%20Leonard',8,'2019-08-05 06:39:09'),(81,3,'Another%20one','Kawhi%20Leonard',8,'2019-08-05 06:45:29'),(82,3,'And%20another%20one','Kawhi%20Leonard',8,'2019-08-05 06:46:48'),(83,3,'skdjfhuks','Kawhi%20Leonard',8,'2019-08-05 06:50:23'),(84,3,'blah%20blah','Kawhi%20Leonard',8,'2019-08-05 06:55:44'),(85,3,'Answers%20a%20question','Kawhi%20Leonard',8,'2019-08-06 06:43:07'),(86,3,'Answer','Kawhi%20Leonard',8,'2019-08-06 06:45:17'),(87,3,'Answer','Kawhi%20Leonard',8,'2019-08-06 06:46:25'),(88,3,'Answerrr','Kawhi%20Leonard',8,'2019-08-06 06:49:29'),(89,3,'avsgdhags','Kawhi%20Leonard',8,'2019-08-06 06:50:40'),(90,3,'angsnd','Kawhi%20Leonard',8,'2019-08-06 21:17:14'),(91,3,'sgdjbsahdgxgjha','Kawhi%20Leonard',8,'2019-08-06 21:20:36'),(92,3,'ggfdgf','Kawhi%20Leonard',8,'2019-08-06 21:57:40'),(93,3,'vdgvfx','Kawhi%20Leonard',8,'2019-08-06 21:58:51'),(94,3,'kajjsdja','Kawhi%20Leonard',8,'2019-08-06 22:26:24'),(95,3,'Hello%20world','Kawhi%20Leonard',9,'2019-08-06 23:27:04'),(96,3,'aghsjdha','Kawhi%20Leonard',9,'2019-08-06 23:27:36'),(97,3,'asajdgga','Kawhi%20Leonard',9,'2019-08-06 23:53:44'),(98,3,'Testing%20notifications','Kawhi%20Leonard',9,'2019-08-07 06:50:11'),(99,3,'Hello%20There%21','Kawhi%20Leonard',8,'2019-08-10 04:41:19'),(100,3,'ashfkajd','Kawhi%20Leonard',9,'2019-08-10 05:22:51'),(101,3,'ANother%20one%0A','Kawhi%20Leonard',9,'2019-08-10 05:23:58'),(102,3,'as%2Cdna','Kawhi%20Leonard',9,'2019-08-10 05:24:49'),(103,3,'asdmnas','Kawhi%20Leonard',9,'2019-08-10 05:25:13'),(104,3,'asmdhb','Kawhi%20Leonard',9,'2019-08-10 05:25:56'),(105,3,'Heyy%20there','Kawhi%20Leonard',9,'2019-08-10 05:38:35'),(106,3,'asdg','Kawhi%20Leonard',9,'2019-08-10 05:39:09'),(107,3,'hstdggfhs','Kawhi%20Leonard',9,'2019-08-10 05:39:17'),(108,3,'asnggdja','Kawhi%20Leonard',9,'2019-08-10 05:39:50'),(109,3,'agdsvfjhbjdbsj%20','Kawhi%20Leonard',9,'2019-08-10 05:40:07');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL auto_increment,
  `sender_id` int(11) default NULL,
  `sender_name` varchar(60) default NULL,
  `receiver_id` int(11) default NULL,
  `reciever_name` varchar(60) default NULL,
  `message` text,
  `isGroup` int(11) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_messages`
--

LOCK TABLES `chat_messages` WRITE;
/*!40000 ALTER TABLE `chat_messages` DISABLE KEYS */;
INSERT INTO `chat_messages` VALUES (1,3,'Kawhi Leonard',5,'Scottie Pippen','ajhsd',0,'2019-08-15 16:51:00'),(2,5,'student student',3,'Kawhi Leonard','Hey',0,'2019-08-15 17:32:15'),(3,3,'Kawhi Leonard',5,'Scottie Pippen','Hello to you too',0,'2019-08-15 17:32:36'),(4,5,'student student',3,'Kawhi Leonard','ahsgghd',0,'2019-08-15 17:33:07'),(5,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:08'),(6,5,'student student',3,'Kawhi Leonard','asd;asd',0,'2019-08-15 17:33:11'),(7,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:12'),(8,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:12'),(9,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:12'),(10,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:12'),(11,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:12'),(12,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:13'),(13,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:13'),(14,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:14'),(15,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:14'),(16,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:14'),(17,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:15'),(18,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:15'),(19,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:15'),(20,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:15'),(21,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:15'),(22,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:15'),(23,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:15'),(24,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:16'),(25,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:16'),(26,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:16'),(27,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:16'),(28,5,'student student',3,'Kawhi Leonard','',0,'2019-08-15 17:33:16'),(29,5,'student student',3,'Kawhi Leonard','Hey Kawhi',0,'2019-08-16 15:35:15'),(30,3,'Kawhi Leonard',5,'Scottie Pippen','Hi,\n\nHow\'s it?',0,'2019-08-16 15:35:38'),(31,5,'student student',3,'Kawhi Leonard','Hey',0,'2019-08-17 19:18:39'),(32,5,'student student',3,'Kawhi Leonard','Hey',0,'2019-08-17 20:45:57'),(33,5,'student student',3,'Kawhi Leonard','askkjdbasgvfs  sagvdas sdfs ',0,'2019-08-17 20:46:23'),(34,5,'student student',8,'undefined','Hey there!',0,'2019-08-28 04:22:27'),(35,8,'Josh Norman',5,'undefined','Hey! How\'s it?',0,'2019-08-28 04:26:11'),(36,5,'student student',8,'undefined','Hi',0,'2019-09-04 20:26:02'),(37,8,'Josh Norman',5,'undefined','Heyy G',0,'2019-09-04 20:26:23'),(38,5,'student student',8,'undefined','Hey there',0,'2019-09-08 11:47:38'),(39,8,'Josh Norman',5,'undefined','How\'s it?',0,'2019-09-08 11:47:53'),(40,5,'student student',8,'undefined','All good! You?',0,'2019-09-08 11:48:48'),(41,5,'student student',8,'undefined','Hey dude',0,'2019-09-08 12:12:43'),(42,8,'Josh Norman',5,'undefined','I am noice!',0,'2019-09-08 12:32:28'),(43,8,'Josh Norman',5,'undefined','Hello there!',0,'2019-09-08 13:00:51'),(44,5,'student student',8,'Josh Norman','Hi',0,'2019-09-08 13:03:53'),(45,8,'Josh Norman',5,'student student','HEyyyy',0,'2019-09-08 13:05:52'),(46,8,'Josh Norman',5,'student student','Hello',0,'2019-09-08 13:14:22'),(47,8,'Josh Norman',5,'student student','Hey you',0,'2019-09-08 13:16:41'),(48,5,'student student',8,'Josh Norman','What\'s good?',0,'2019-09-08 13:16:55'),(49,5,'student student',8,'Josh Norman','Hey there',0,'2019-09-08 13:39:37'),(50,8,'Josh Norman',5,'student student','Wassssuuuup',0,'2019-09-08 13:39:46'),(51,5,'student student',8,'Josh Norman','All good man! wassup?',0,'2019-09-08 13:40:07'),(52,5,'student student',8,'Josh Norman','W',0,'2019-09-08 13:41:27'),(53,5,'student student',8,'Josh Norman','e',0,'2019-09-08 13:41:30'),(54,5,'student student',8,'Josh Norman','s',0,'2019-09-08 13:41:32'),(55,5,'student student',8,'Josh Norman','t',0,'2019-09-08 13:41:35');
/*!40000 ALTER TABLE `chat_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) default NULL,
  `username` varchar(255) default NULL,
  `comment` text,
  `note_id` int(11) default NULL,
  `date_time` varchar(60) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,4,'Ben Simmons','Test comment!',0,'1548604365'),(2,4,'Ben Simmons','Test',0,'1548604418'),(3,4,'Ben Simmons','Test',0,'1548604493'),(4,4,'Ben Simmons','Test',0,'1548604560'),(5,4,'Ben Simmons','Test 2',0,'1548604574'),(6,4,'Ben Simmons','Yo!! I used .to love that jam bruh!',11,'1548605613'),(7,4,'Ben Simmons','Oh! Is it really?',8,'1548606093'),(8,4,'Ben Simmons','Spending Gs',10,'1548859213'),(9,4,'Ben Simmons','Comment #2!',10,'1548859699'),(10,4,'Ben Simmons','Comment #1',1,'1548859731'),(11,4,'Ben Simmons','Comment #2',1,'1548859738'),(12,6,'Lebron James','You&#039;re playing, boy! Come to LA.',1,'1548870040');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grade`
--

DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `id` int(11) NOT NULL,
  `name` varchar(255) default NULL,
  `year` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade`
--

LOCK TABLES `grade` WRITE;
/*!40000 ALTER TABLE `grade` DISABLE KEYS */;
/*!40000 ALTER TABLE `grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) default NULL,
  `admin_id` int(11) default NULL,
  `admin_name` varchar(60) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Maths group',2,'Nemanja Nedovic','2019-08-20 17:12:04');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) default NULL,
  `username` varchar(255) default NULL,
  `note_id` int(11) default NULL,
  `date_time` varchar(60) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (1,4,'Ben Simmons',1,'1548864136'),(2,4,'Ben Simmons',1,'1548864985'),(3,4,'Ben Simmons',10,'1548865442'),(4,4,'Ben Simmons',10,'1548865448'),(5,4,'Ben Simmons',19,'1548865849');
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `author` varchar(255) default NULL,
  `user_id` int(11) default NULL,
  `title` varchar(255) default NULL,
  `subject` varchar(60) default NULL,
  `content` text,
  `language` varchar(60) default NULL,
  `grade` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
INSERT INTO `notes` VALUES (1,'Ben Simmons',4,'Test','Mathematics ','','English',11),(2,'Ben Simmons',4,'Test','Mathematics ','','English',11),(3,'Ben Simmons',4,'Test 2','Mathematics ','<p>This is another test</p>\n','English',11),(4,'Ben Simmons',4,'Test 3 ','Mathematics ','<p>Test 3</p>\n','English',11),(5,'Ben Simmons',4,'Test 3 ','Mathematics ','<p>Test 3</p>\n','English',11),(6,'Ben Simmons',4,'Test 4','Mathematics ','<p>Test 4</p>\n','English',11),(7,'Ben Simmons',4,'Hey Hey','Mathematics ','<p>This is a test</p>\n','English',11),(8,'Ben Simmons',4,'Hey Hey','Mathematics ','<p>This is a test</p>\n','English',11),(9,'Ben Simmons',4,'Hello World','Mathematics ','<p>Hello world.</p>\n','English',11),(10,'Ben Simmons',4,'Big Pimpin','Mathematics ','<p>We be ...</p>\n','English',11),(11,'Ben Simmons',4,'Big Pimpin','Mathematics ','<p>We be ...</p>\n','English',11),(12,'Ben Simmons',4,'Real Rap','Mathematics ','<p>Don&#39;t come up</p>\n','English',11),(13,'Ben Simmons',4,'Another day another dollar!','Mathematics ','<p>Avukile, ayophanda amajimbozi.</p>\n','IsiZulu',11),(14,'Ben Simmons',4,'Premier Gaou','Mathematics ','<p>jsdghfa</p>\n','IsiXhosa',11),(15,'Ben Simmons',4,'Phfbhg','Mathematics ','<p>dfhcgkdl.xc</p>\n','IsiXhosa',11),(16,'Ben Simmons',4,'Test color','Mathematics ','<p>This is to test the color of the server response.</p>\n','English',11),(17,'Ben Simmons',4,'Another Color Test','Mathematics ','<p>This is another color test</p>\n','English',11),(18,'Ben Simmons',4,'Testing color fi di error','Mathematics ','<p>Testing red</p>\n','English',11),(19,'Ben Simmons',4,'red test','Mathematics ','<p>testing</p>\n','English',11);
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `notification` text,
  `is_read` int(11) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(2,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(3,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(4,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(5,3,'Kawhi Leonard has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(6,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(7,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(8,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(9,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(10,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(11,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(12,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(13,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(14,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'0000-00-00 00:00:00'),(15,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'2019-06-09 13:17:41'),(16,3,'Sanele Mpangalala has answered your question. Go to Mathematics Grade 10',1,'2019-06-15 14:54:39'),(17,1,'student student has answered your question. Go to Mathematics Grade 10',0,'2019-07-26 16:01:39'),(18,5,'student student has answered your question. Go to mathematics Grade 10',1,'2019-07-28 00:25:38'),(19,5,'Sanza Mananza has answered your question. Go to mathematics Grade 10',1,'2019-07-29 16:24:32'),(20,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-05 06:39:10'),(21,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-05 06:45:29'),(22,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-05 06:46:48'),(23,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-05 06:50:23'),(24,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-05 06:55:44'),(25,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 06:43:07'),(26,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 06:45:17'),(27,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 06:46:25'),(28,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 06:49:29'),(29,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 06:50:40'),(30,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 21:17:14'),(31,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 21:20:36'),(32,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 21:57:41'),(33,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 21:58:51'),(34,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-06 22:26:24'),(35,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-06 23:27:04'),(36,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-06 23:27:36'),(37,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-06 23:53:44'),(38,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-07 06:50:11'),(39,5,'Kawhi Leonard has answered your question. Go to mathematics Grade 10',1,'2019-08-10 04:41:19'),(40,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:22:51'),(41,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:23:58'),(42,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:24:49'),(43,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:25:13'),(44,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:25:56'),(45,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:38:35'),(46,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:39:09'),(47,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:39:17'),(48,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:39:50'),(49,5,'Kawhi Leonard has answered your question. Go to physics Grade 10',1,'2019-08-10 05:40:07');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `question_id` int(10) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `subject` varchar(255) default NULL,
  `question` text,
  `username` varchar(255) default NULL,
  `grade` int(11) default NULL,
  `topic` varchar(255) default NULL,
  `language` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,2,'Mathematics','test question updated v5','Namanja Nedovic',10,'test topic','English','0000-00-00 00:00:00'),(2,2,'Mathematics','Testing before deploying','Namanja Nedovic',10,'Test prod','IsiXhosa','0000-00-00 00:00:00'),(3,1,'Mathematics','Hello???','Sanele Mpangalala',10,'Test 3','English','0000-00-00 00:00:00'),(4,2,'Mathematics','Testing\n','Namanja Nedovic',10,'Namanja Test','English','0000-00-00 00:00:00'),(5,4,'Mathematics','Test%20question','Lou%20Will',10,'akshd','English','0000-00-00 00:00:00'),(6,3,'Mathematics','Testing%20sockets','Kawhi%20Leonard',10,'Sockets','English','0000-00-00 00:00:00'),(7,1,'Mathematics','What%27s%20good%3F','Sanele%20Mpangalala',10,'','English','2019-06-18 20:19:39'),(8,5,'mathematics','nhdbjfm kdsufksl','student%20student',10,'JHjs','English','2019-07-26 17:59:11'),(9,5,'physics','Test','student%20student',10,'Test','English','2019-08-06 23:26:17');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_group`
--

DROP TABLE IF EXISTS `student_group`;
CREATE TABLE `student_group` (
  `id` int(11) NOT NULL auto_increment,
  `student_id` int(11) default NULL,
  `group_id` int(11) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_group`
--

LOCK TABLES `student_group` WRITE;
/*!40000 ALTER TABLE `student_group` DISABLE KEYS */;
INSERT INTO `student_group` VALUES (2,1,5,'2019-08-20 20:29:32'),(3,5,3,'2019-08-20 20:37:36'),(4,5,1,'2019-08-20 20:38:29');
/*!40000 ALTER TABLE `student_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_tutor`
--

DROP TABLE IF EXISTS `student_tutor`;
CREATE TABLE `student_tutor` (
  `id` int(11) NOT NULL auto_increment,
  `student_id` int(11) default NULL,
  `tutor_id` int(11) default NULL,
  `pending` int(11) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_tutor`
--

LOCK TABLES `student_tutor` WRITE;
/*!40000 ALTER TABLE `student_tutor` DISABLE KEYS */;
INSERT INTO `student_tutor` VALUES (1,5,2,NULL,'2019-08-20 06:55:17'),(2,5,1,NULL,'2019-08-20 21:22:46'),(3,3,1,NULL,'2019-08-20 21:24:16'),(4,3,7,1,'2019-08-25 11:13:55'),(5,3,7,1,'2019-08-25 11:20:50'),(6,3,7,1,'2019-08-25 11:21:00'),(7,5,8,0,'2019-09-08 11:21:21'),(8,7,7,1,'2019-09-08 00:30:27');
/*!40000 ALTER TABLE `student_tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) default NULL,
  `grade` int(11) default NULL,
  `learning_area` varchar(60) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (1,'Maths - Grade 10',10,'mathematics'),(2,'Maths - Grade 11',11,'mathematics'),(3,'Maths - Grade 12',12,'mathematics'),(4,'Physics - Grade 10',10,'physical sciences'),(5,'Physics - Grade 11',11,'physical sciences'),(6,'Physics - Grade 12',12,'physical sciences');
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,'Sanele Mpangalala');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_subject`
--

DROP TABLE IF EXISTS `tutor_subject`;
CREATE TABLE `tutor_subject` (
  `id` int(11) NOT NULL auto_increment,
  `tutor_id` int(11) default NULL,
  `subject_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutor_subject`
--

LOCK TABLES `tutor_subject` WRITE;
/*!40000 ALTER TABLE `tutor_subject` DISABLE KEYS */;
INSERT INTO `tutor_subject` VALUES (1,8,1),(2,8,2),(3,8,3),(4,8,4),(5,8,5),(6,8,6);
/*!40000 ALTER TABLE `tutor_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutorials`
--

DROP TABLE IF EXISTS `tutorials`;
CREATE TABLE `tutorials` (
  `tut_id` int(11) NOT NULL,
  `title` varchar(255) default NULL,
  `subject` varchar(127) default NULL,
  `description` varchar(127) default NULL,
  `grade` int(11) default NULL,
  `language` varchar(127) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutorials`
--

LOCK TABLES `tutorials` WRITE;
/*!40000 ALTER TABLE `tutorials` DISABLE KEYS */;
INSERT INTO `tutorials` VALUES (1,'Test','Mathematics ','Testing things',12,'English'),(2,'Another Test','Mathematics ','This is another test for the tutorials feature.',10,'English'),(3,'Test3','Physical Sciences ','Ya know what it is',10,'English'),(4,'TEst 4','Mathematics ','Testing tut folder',10,'English'),(5,'Test 5','Mathematics ','Changed the way we name tutrials',10,'English'),(6,'Test 6','Mathematics ','Okay now!',10,'English'),(7,'Test 6','Mathematics ','Okay now!',10,'English');
/*!40000 ALTER TABLE `tutorials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(10) NOT NULL auto_increment,
  `first_name` varchar(255) default NULL,
  `last_name` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `cell_number` varchar(255) default NULL,
  `user_password` varchar(255) default NULL,
  `user_role` varchar(255) default NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `cell_number` (`cell_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Sanele','Mpangalala','mpnsan005@myuct.ac.za','91274edb1457259ea5f9ddaf52c08730','2c9a141e9847d7b91869811eca56f724','student','0000-00-00 00:00:00'),(2,'Namanja','Nedovic','namanja@nedovic.com','ec95d3646dfddfed313d8aba0d4d785c','098f6bcd4621d373cade4e832627b4f6','teacher','0000-00-00 00:00:00'),(3,'Kawhi','Leonard','kawhi@gmail.com','57287d5f736533100821b87cae1843b0','fd4e00f4245fe143b43e2fa1c8d9c538','student','0000-00-00 00:00:00'),(4,'Lou','Will','test@test.com','216b817cc54918cd3fafd77c61853370','5d41402abc4b2a76b9719d911017c592','student','0000-00-00 00:00:00'),(5,'student','student','student@gmail.com','065071f93ea35ad7195e1d3046d008fe','cd73502828457d15655bbd7a63fb0bc8','student','2019-07-07 08:03:02'),(6,'Sanza','Mananza','mananza@gmail.com','a392f1b722157579489797723e3713bc','1f0ec04c6fef94134fdcb4ee95f3fefc','student','2019-07-29 16:23:31'),(7,'Sanele','Mpangalala','tutor@gmail.com','dc4e0ace687a4b900035c052054b8369','1f6f42334e1709a4e0f9922ad789912b','tutor','2019-08-20 21:59:13'),(8,'Josh','Norman','josh@gmail.com','26ed1d1548964d2c4ccd75cee086846e','f94adcc3ddda04a8f34928d862f404b4','tutor','2019-08-25 15:43:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-10 20:48:54

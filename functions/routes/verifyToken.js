const jwt = require('jsonwebtoken');
module.exports = function(req, res, next) {
    const token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({error: 'Access Denied'});

    try {
        const user = jwt.verify(token, "BaGUvIx");
        req.user = user;
        // eslint-disable-next-line callback-return
        next();
    } catch(err) {
        res.status(400).send({error: 'Invalid Token'});
    }
}

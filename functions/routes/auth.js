const router = require('express').Router();
const jwt = require('jsonwebtoken');
const User = require('../models/user');

router.post('/login', (request, response) => {
    // get user details
    const username = request.body.username;
    const password = request.body.password;
    // connect to DB

    User.findOne({'username': username, 'password': password},'username name lastName role profileImageUrl', (err, user) => {
        if (err) response.send({error: 'Login Failed. Contact Administrator.'});
        if (!user) {
            response.send({error: 'No match for username and password'})
        } else {
            const token = jwt.sign({_id: user._id}, "BaGUvIx");
            response.header('x-access-token', token).send({
                data: user
            });
        }
    });
});

router.post('/register', (request, response) => {
    const body = request.body;
    if (body.password === body.passwordConfirmation) {
        delete body['passwordConfirmation'];
        const user = new User(body);
        user.save((err, user) => {
            if (err){
                console.log(err)
                response.send({error: 'Registration Failed'});
            }else {
                response.status(200).send({});
            }
        });
    } else {
        response.send({
            message: 'passwords-do-not-match'
        });
    }
});

module.exports = router;

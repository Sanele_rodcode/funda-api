const router = require("express").Router();
const path = require("path");
const os = require("os");
const fs = require("fs");
const verify = require('./verifyToken');
const User = require("../models/user");
const Chat = require("../models/chat");
const index = require('../index');

const Busboy = require("busboy");

const {Storage} = require("@google-cloud/storage");

const storage = new Storage({
    projectId: "serious-amulet-267419",
    keyFilename: path.join(
        __dirname,
        "../serious-amulet-267419-2d2db481b31d.json"
    )
});

const bucket = storage.bucket("fundaapp");

router.post("/update", verify,(request, response) => {
    const id = request.user._id;
    const user = request.body;
    User.update(
        {_id: id},
        user,
        {upsert: false},
        (err, user) => {
            if (err) {
                response.send({error: 'Something went wrong'})
            } else {
                delete user['password'];
                response.status(200).send({data: user})
            }
        })
});

router.post("/update/avatar",verify, (req, response) => {
    if (req.method !== "POST") {
        // Return a "method not allowed" error
        return res.status(405).end();
    }
    const busboy = new Busboy({headers: req.headers});
    const tmpdir = os.tmpdir();

    // This object will accumulate all the uploaded files, keyed by their name.
    const uploads = {};

    const fileWrites = [];
    let userId = req.user._id;
    let name;
    let filepath;
    busboy.on("file", (fieldname, file, filename) => {
        filepath = path.join(tmpdir, filename);
        name = filename;
        uploads[fieldname] = filepath;

        const writeStream = fs.createWriteStream(filepath);
        file.pipe(writeStream);

        // File was processed by Busboy; wait for it to be written to disk.
        const promise = new Promise((resolve, reject) => {
            file.on("end", () => {
                writeStream.end();
            });
            writeStream.on("finish", resolve);
            writeStream.on("error", reject);
        });
        fileWrites.push(promise);
    });

    // Triggered once all uploaded files are processed by Busboy.
    // We still need to wait for the disk writes (saves) to complete.
    busboy.on("finish", async () => {
        await Promise.all(fileWrites)
            .then(fw => {
                // TODO(developer): Process saved files here
                // eslint-disable-next-line promise/no-nesting
                bucket
                    .upload(filepath, {
                        contentType: "image/*"
                    })
                    .then(uploadRes => {
                        // eslint-disable-next-line promise/no-nesting
                        User.update({_id: userId},{profileImageUrl: `https://storage.googleapis.com/fundaapp/${name}`}, {upsert: false},
                            (err, result) => {
                                if (err) {
                                    response.send({error: "Something went wrong"});
                                } else {
                                    response.status(200).send({data: `https://storage.googleapis.com/fundaapp/${name}`})
                                }
                            });
                        return uploadRes;
                    })
                    .catch(err => console.log(err));
                return fw;
            })
            .catch(err => console.log(err));
    });

    req.pipe(busboy);
});

router.post("/chats", verify, (request, response) => {
    const userID = request.user._id;
    const body = request.body;
    const chat = new Chat(body);
    chat.save((err, chat) => {
        if (err) {
            response.send({error: 'Registration Failed'});
        } else {
            response.status(200).send({});
        }
    });
});

router.get("/chats", verify, (request, response) => {
    const id = request.user._id;
    Chat.find({"users._id": id}, (err, savedChats) => {
        if (err) {
            response.send({error: "Something went wrong"});
        } else {
            let chats = savedChats;
            chats.forEach(chat => {
                chat.users = chat.users.map(user => {
                    const contactFound = index.onlineUsers.find(el => el.id === user._id);
                    if (contactFound) {
                        user.online = true;
                    }
                    return user;
                });
            });
            response.status(200).send({data: chats});
        }
    })
});

router.get("/contacts", verify, (request, response) => {
    const id = request.user._id;
    User.findOne({_id: id}, (err, user) => {
        if (err) {
            response.send({error: 'Something went wrong'})
        } else {
            let contacts = user.contacts;
            contacts.forEach(contact => {
                const contactFound = index.onlineUsers.find(user => user.id === contact._id);
                if (contactFound) {
                    contact.online = true;
                }
            });
            response.send({data: contacts})
        }
    })
});

router.get("/invites", verify, (request, response) => {
    const id = request.user._id;
    User.findOne({_id: id}, (err, user) => {
        if (err) {
            response.send({error: 'Something went wrong'})
        } else {
            response.send({data: user.invites})
        }
    })
});

router.get("/invitees", verify, (request, response) => {
    const id = request.user._id;
    User.findOne({_id: id}, (err, user) => {
        if (err) {
            response.send({error: 'Something went wrong'})
        } else {
            response.send({data: user.invitees})
        }
    })
});

router.post("/invites/accept", verify, (request, response) => {
    const id = request.user._id;
    const invite = request.body.invite;
    User.findOneAndUpdate(
        {_id: id},
        {
            $pull: {invites: {_id: invite._id}},
            $push: {
                contacts: {
                    _id: invite._id,
                    fullName: invite.fullName,
                    role: invite.role,
                    profileImageUrl: invite.profileImageUrl
                }
            }
        },
        {upsert: false},
        (err, user) => {
            if (err) {
                response.send({error: 'Something went wrong'});
            } else {
                User.update(
                    {_id: invite._id},
                    {
                        $pull: {invitees: {_id: user._id}},
                        $push: {
                            contacts: {
                                _id: user._id,
                                fullName: `${user.name} ${user.lastName}`,
                                role: user.role,
                                profileImageUrl: user.profileImageUrl
                            }
                        }
                    },
                    {upsert: false},
                    (err, res) => {
                        if (err) {
                            response.send({error: "Something went wrong"});
                        } else {
                            response.status(200).send({});
                        }
                    });
            }
        }
    )
});
router.post("/invites/decline", verify, (request, response) => {
    const id = request.user._id;
    const invite = request.body.invite;
    User.findOneAndUpdate(
        {_id: id},
        {
            $pull: {invites: {_id: invite._id}}
        },
        {upsert: false},
        (err, user) => {
            if (err) {
                response.send({error: 'Something went wrong'});
            } else {
                User.update(
                    {_id: invite._id},
                    {
                        $pull: {
                            invitees: {
                                _id: user._id
                            }
                        }
                    },
                    {upsert: false},
                    (err, res) => {
                        if (err) {
                            response.send({error: "Something went wrong"});
                        } else {
                            response.status(200).send({});
                        }
                    });
            }
        }
    )
});

router.post("/contacts/add", verify, (request, response) => {
    const id = request.user._id;
    const user = request.body.user;

    User.findOneAndUpdate({_id: id}, {
        $push: {
            invitees: {
                _id: user._id,
                fullName: `${user.name} ${user.lastName}`,
                role: user.role,
                profileImageUrl: user.profileImageUrl
            }
        }
    }, {upsert: false}, (err, me) => {
        if (err) {
            response.send({error: 'Something went wrong'});
        } else {
            User.update(
                {_id: user._id},
                {
                    $push: {
                        invites: {
                            _id: me._id,
                            fullName: `${me.name} ${me.lastName}`,
                            role: me.role,
                            profileImageUrl: me.profileImageUrl
                        }
                    }
                },
                {upsert: false},
                (err, res) => {
                    if (err) {
                        response.send({error: 'Something went wrong'});
                    } else {
                        response.status(200).send({});
                    }
                }
            )
        }
    });
});

router.post("/contacts/remove", verify, (request, response) => {
    const id = request.user._id;
    const userID = request.body.userID;

    User.update({_id: id}, {
        $pull: {
            contacts: {
                _id: userID
            }
        }
    }, {upsert: false}, (err, me) => {
        if (err) {
            response.send({error: 'Something went wrong'});
        } else {
            response.status(200).send({});
        }
    });
});

router.get("/search", verify, (request, response) => {
    const id = request.user._id;
    const query = request.query["query"];
    const filterText = request.query["filterText"] === "all" ? "" : request.query["filterText"];
    const subject = request.query["filterSubject"];
    const grade = request.query["grade"];
    User.find({
            $or: [{name: {$regex: query, $options: 'i'}, role: {$regex: filterText}}, {
                lastName: {
                    $regex: query,
                    $options: 'i'
                },
                role: {$regex: filterText}
            }]
        }, '_id name lastName profileImageUrl role subjects location',
        (err, users) => {
            if (err) {
                response.send({error: 'Something Went Wrong'});
            } else {
                const filteredUsers = users.filter(user => {
                   if (!user._id.equals(id)) {
                       if (subject !== '') {
                           const foundSubject = user.subjects.find(el => el._id.equals(subject));
                           console.log(foundSubject);
                           if (foundSubject) {
                               return true
                           } else {
                               return false;
                           }
                       }
                       return true;
                   } else {
                       return false;
                   }
                });

                response.send({data: filteredUsers});
            }
        });
});

router.get('/:id', verify, (request, response) => {
   const userId = request.params.id;

    // eslint-disable-next-line promise/catch-or-return,promise/always-return
   User.findOne({_id: userId}).populate('subjects').exec().then(user => {
       response.send({
           data: user
       })
   });
});


module.exports = router;

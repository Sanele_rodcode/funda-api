const router = require("express").Router();
const Subject = require("../models/subject");

router.get("/", (request, response) => {
  Subject.find((err, subjects) => {
    if (err) response.send({error: 'Could not retrieve subjects'});

    response.send({data: subjects});
  });
});

router.post("/", (request, response) => {
  const subjects = request.body;

  Subject.insertMany(subjects, (err, subjects) => {
    if (err) response.send({error: 'Could Not Save Subjects'});

    response.status(200).send({});
  });
});

module.exports = router;

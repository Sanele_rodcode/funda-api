const router = require("express").Router();
const admin = require('firebase-admin');
const verify = require('./verifyToken');

router.get("/",verify, (request, response) => {
    const query = request.query['query'];
    const userId = request.user._id;

});

router.post("/:lessonId/enroll", verify, (request, response) => {
    const lessonId = request.param("lessonId");
    const user = request.body.user; // need to use the id to fetch  user

});

router.get("/my-lessons", verify, (request, response) => {
  const userId = request.user._id;

});
module.exports = router;

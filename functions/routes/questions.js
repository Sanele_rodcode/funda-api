const router = require('express').Router();
const Question = require("../models/question");

router.get('/', (request, response) => {
    const subject = request.query['subject'];
    const grade = request.query['grade'];
    // eslint-disable-next-line promise/catch-or-return,promise/always-return
    Question.find({'subject': subject, 'grade': grade}).populate('subject').populate('asker').exec().then(questions => {
        response.send({
            data: questions
        })
    });
});

module.exports = router;

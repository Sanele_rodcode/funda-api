const router = require('express').Router();
const Question = require("../models/question");
router.post('/create', (request, response) => {
    const question = new Question(request.body);
    question.save(question, (err, question) => {
        if (err) {
            console.log(err)
            response.send({error: 'Could Not Save Question'});
        } else {
            response.send({
                data: question
            })
        }
    });
});

router.post('/update', (request, response) => {
    const id = request.body._id;
    console.log(id)
    Question.findOneAndUpdate({_id: id}, { $set: {answers: request.body.answers}}, {upsert: true}, (err, doc) => {
        if (err) {
            console.log(err)
            response.send({error: 'Could Not Update Question'})
        } else {
            response.send({data: doc})
        }
    });
});

module.exports = router;

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27018/funda', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const Schema = mongoose.Schema;

const subjectSchema = new Schema({
    _id: {type: mongoose.Schema.Types.ObjectId},
    name: String
});

const Subject = mongoose.model('Subject', subjectSchema);

module.exports = Subject;

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27018/funda', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const Schema = mongoose.Schema;

const locationSchema = new Schema({
    city: String,
    province: String
});

const userSchema = new Schema({
    name: String,
    lastName: String,
    username: String,
    role: String,
    email: String,
    cellNo: String,
    password: String,
    passwordConfirmation: String,
    location: locationSchema,
    profileImageUrl: String,
    grade: String,
    gender: String,
    school: String,
    dob: String,
    subjects: [{type: mongoose.Types.ObjectId, ref: 'Subject'}],
    online: String,
    invites: [{_id: String, fullName: String, role: String, profileImageUrl: String}],
    invitees: [{_id: String, fullName: String, role: String, profileImageUrl: String}],
    contacts: [{_id: String, fullName: String, role: String, profileImageUrl: String, online: Boolean}]
});

const User = mongoose.model('User', userSchema);

module.exports = User;

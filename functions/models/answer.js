const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27018/funda', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const Schema = mongoose.Schema;

const answerSchema = new Schema({
    answerer: {type: mongoose.Types.ObjectId, ref: 'User'},
    answer: String,
    date: String,
    likes: [{type: mongoose.Types.ObjectId, ref: 'User'}],
});

const Answer = mongoose.model('Answer', answerSchema);

module.exports = Answer;

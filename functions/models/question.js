const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27018/funda', {useNewUrlParser: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const Schema = mongoose.Schema;

const questionSchema = new Schema({
    _id: {type: mongoose.Schema.Types.ObjectId},
    asker: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    question: String,
    answers: [{
        answerer: {type: mongoose.Types.ObjectId, ref: 'User'},
        answer: String,
        date: String,
        likes: [{type: mongoose.Types.ObjectId, ref: 'User'}]
    }],
    date: String,
    subject: {type: mongoose.Types.ObjectId, ref: 'Subject'},
    grade: String,
    topic: String,
});

const Question = mongoose.model('Question', questionSchema);

module.exports = Question;

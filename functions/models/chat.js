const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27018/funda', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const Schema = mongoose.Schema;

const chatSchema = new Schema({
    lastMessage: String,
    dateModified: String,
    read: Boolean,
    users: [{_id: String, fullName: String, role: String, profileImageUrl: String, online: Boolean}],
    receivingUser: {_id: String, fullName: String, role: String, profileImageUrl: String, online: Boolean},
    messages: [{
        sender: {_id: String, fullName: String, role: String, profileImageUrl: String},
        receiver: {_id: String, fullName: String, role: String, profileImageUrl: String},
        content: String,
        timestamp: Number
    }],
    totalMessages: Number,
});

module.exports = mongoose.model('Chat', chatSchema);

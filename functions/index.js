/* eslint-disable promise/always-return */
const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");
const Chat = require("./models/chat");

const app = express();
const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/user");
const subjectsRoutes = require("./routes/subjects");
const questionRoutes = require("./routes/question");
const questionsRoutes = require("./routes/questions");
const lessonsRoutes = require("./routes/lessons");

const server = http.createServer(app);
var io = require("socket.io")(server);

app.use(bodyParser.json());

app.use((req, res, next) => {
    const allowedOrigins = ["http://localhost", "http://localhost:8100", "*"];
    const origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader("Access-Control-Allow-Origin", origin);
    }

    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Expose-Headers", "x-access-token");
    next();
});

// Routes
app.use("/api/auth", authRoutes);
app.use("/api/user", userRoutes);
app.use("/api/subjects", subjectsRoutes);
app.use("/api/question", questionRoutes);
app.use("/api/questions", questionsRoutes);
app.use("/api/lessons", lessonsRoutes);

// Sockets
let onlineUsers = [];
io.on("connection", socket => {
    const onlineUser = onlineUsers.find(
        user => user.id === socket.handshake.query.userID
    );
    socket.broadcast.emit("userOnline", socket.handshake.query.userID);

    if (onlineUser) {
        onlineUser.socket = socket.id;
    } else {
        onlineUsers.push({id: socket.handshake.query.userID, socket: socket.id});
    }

    socket.on("outgoingMessage", data => {
        const chatId = data.chatId;
        let chat;
        if (chatId) {
            Chat.findOneAndUpdate({_id: chatId}, {$push: {messages: data.message}, lastMessage: data.message.content},
                {upsert: false}, (err, chat) => {
                    if (err) {
                        io.to(socket.id).emit("messageReceived", {
                            status: "fail",
                            message: data.message
                        });
                    } else {
                        if (onlineUsers.find(user => user.id === data.message.receiver._id)) {
                            io.to(onlineUsers.find(user => user.id === data.message.receiver._id).socket)
                                .emit("incomingMessage", {
                                    status: "success",
                                    message: data.message,
                                    chatId: chat._id,
                                    lastMessage: data.message.content
                                });
                        }
                        io.to(socket.id).emit("messageReceived", {
                            status: "success",
                            message: data.message,
                            chatId: chat._id,
                            lastMessage: data.message.content
                        });

                    }
                });
        } else {
            console.log(data)
            chat = new Chat({lastMessage: data.message.content, messages: [data.message], users: data.users});
            console.log(chat);
            chat.save((err, chat) => {
                if (err) {
                    io.to(socket.id).emit("messageReceived", {
                        status: "fail",
                        message: data.message
                    });
                } else {
                    const sent = io.to(onlineUsers.find(user => user.id === data.message.receiver._id).socket)
                        .emit("incomingMessage", {
                            status: "success",
                            message: data.message,
                            chatId: chat._id,
                            lastMessage: data.message.content
                        });
                    if (sent) {
                        io.to(socket.id).emit("messageReceived", {
                            status: "success",
                            message: data.message,
                            chatId: chat._id,
                            lastMessage: data.message.content
                        });
                    }
                }
            });
        }
    });

    socket.on("disconnect", () => {
        const loggedOutUser = onlineUsers.find(user => user.socket === socket.id) ? onlineUsers.find(user => user.socket === socket.id).id : null;
        socket.broadcast.emit(
            "userOffline",
            loggedOutUser
        );
        onlineUsers = onlineUsers.filter(user => user.socket !== socket.id);
    });
});

server.listen(3000, () => {
    console.log("server started");
});

module.exports.onlineUsers = onlineUsers;
